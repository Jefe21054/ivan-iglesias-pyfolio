<div align="center">
  <h1 align="center">My Modern Web Portfolio</h1>

  <div>
    <img src="https://img.shields.io/badge/Python-3.11+-yellow?style=for-the-badge&logo=python&logoColor=white&labelColor=101010" alt="python" />
    <img src="https://img.shields.io/badge/Reflex-0.4.5+-5646ED?style=for-the-badge&logo=reflex&logoColor=white&labelColor=101010" alt="reflex" />
    <img src="https://img.shields.io/badge/-Tailwind_CSS-black?style=for-the-badge&logoColor=white&logo=tailwindcss&color=06B6D4" alt="tailwindcss" />
  </div>
</div>

## 📋 <a name="table">Table of Contents</a>

1. 🤖 [Introduction](#introduction)
2. ⚙️ [Tech Stack](#tech-stack)
3. 🔋 [Features](#features)
4. 🤸 [Quick Start](#quick-start)
6. 🔗 [Assets](#links)

## <a name="introduction">🤖 Introduction</a>

Built with Reflex, a Python framework that allows you to create beautiful and user-friendly websites, and styled with TailwindCSS, this portfolio demonstrates the developer's skills in a unique manner that creates a lasting impact.

## <a name="tech-stack">⚙️ Tech Stack</a>

- Python
- Reflex
- Tailwind CSS

## <a name="features">🔋 Features</a>

👉 **Hero**: Captivating introduction featuring a simple and understandable background.

👉 **Minimalist Elements**:  Interactive design elements, adding depth and engagement.

👉 **Technologies**: Dynamic testimonials area with scrolling or animated content for enhanced engagement.

👉 **Work Experience**: Prominent display of professional background for emphasis and credibility.

👉 **Responsiveness**: Seamless adaptability across all devices, ensuring optimal viewing experience for every user.

and many more, including code architecture and reusability 

## <a name="quick-start">🤸 Quick Start</a>

Follow these steps to set up the project locally on your machine.

**Prerequisites**

Make sure you have the following installed on your machine:

- [Git](https://git-scm.com/)
- [Python](https://python.org)
- [Reflex](https://reflex.dev) 

**Cloning the Repository**

```bash
git clone https://gitlab.com/Jefe21054/ivan-iglesias-pyfolio.git
cd ivan-iglesias-pyfolio
```

**Installation**

Install the project dependencies using pip:

```bash
pip install -r requirements.txt
```

**Running the Project**

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) in your browser to view the project.


## <a name="links">🔗 Assets</a>

Assets used in the project can be found [here](https://drive.google.com/file/d/1ZmtiMilUYTp1wkiXWMFX6AUk-msE981-/view?usp=sharing)

#